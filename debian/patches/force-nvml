Description: Force nvml configuration without autodetection
 This patch will force nvml configuration without autodetection at build
 time. This is necessary to enable the nvml contrib plugin
Author: Gennaro Oliva <oliva.g@na.icar.cnr.it>
Origin: vendor
Forwarded: https://bugs.schedmd.com/show_bug.cgi?id=15909
Last-Update: 2024-07-21

--- a/src/interfaces/gpu.c
+++ b/src/interfaces/gpu.c
@@ -93,7 +93,6 @@
 	uint32_t autodetect_flags = gres_get_autodetect_flags();
 
 	if (autodetect_flags & GRES_AUTODETECT_GPU_NVML) {
-#ifdef HAVE_NVML
 		(void) dlerror();
 		if (!(ext_lib_handle = dlopen("libnvidia-ml.so",
 					      RTLD_NOW | RTLD_GLOBAL)) &&
@@ -103,9 +102,6 @@
 			     dlerror());
 		else
 			return "gpu/nvml";
-#else
-		info("We were configured to autodetect nvml functionality, but we weren't able to find that lib when Slurm was configured.");
-#endif
 	} else if (autodetect_flags & GRES_AUTODETECT_GPU_RSMI) {
 #ifdef HAVE_RSMI
 		(void) dlerror();
