Description: Retry getaddrinfo five times before giving up
 This patch retry getaddrinfo five times before giving up and exiting SLURM
 deamons. This is needed on Debian systems when using ifupdown for network
 configuration with allow-hotplug interfaces (which the installer uses by
 default). This settings do not guarantee that getaddrinfo succeed after
 network-online.target is complete.
Author: Gennaro Oliva <oliva.g@na.icar.cnr.it>
Forwarded: https://bugs.schedmd.com/show_bug.cgi?id=13280
Last-Update: 2024-12-01

--- a/src/common/util-net.c
+++ b/src/common/util-net.c
@@ -39,6 +39,7 @@
 
 #define _GNU_SOURCE
 
+#include <unistd.h>
 #include <arpa/inet.h>
 #include <errno.h>
 #include <limits.h>	/* for PATH_MAX */
@@ -258,8 +259,15 @@
 {
 	struct addrinfo *result = NULL;
 	int err;
+	int retry = 10;
+
+	while ( ( (err = getaddrinfo(hostname, serv, hints, &result)) != 0 ) && retry ) {
+		error("%s: getaddrinfo() failed: %s: %m, attempt number %d", __func__,
+			gai_strerror(err), 6 - retry);
+		sleep(1);
+		retry -= 1;
+	}
 
-	err = getaddrinfo(hostname, serv, hints, &result);
 	if (err == EAI_SYSTEM) {
 		error_in_daemon("%s: getaddrinfo(%s:%s) failed: %s: %m",
 				__func__, hostname, serv, gai_strerror(err));
